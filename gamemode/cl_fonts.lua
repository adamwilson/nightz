--[[

	The purpose of this file is to hold all the font creation

]]
surface.CreateFont("MenuFont", {
	font = "Angelina",
	size = 34,
	weight = 800,
})

surface.CreateFont("MenuBodyFont", {
	font = "Angelina",
	size = 28,
	weight = 400,
})

surface.CreateFont("closeButton", {
	font = "Default",
	size = 16,
})

surface.CreateFont("tickButton", {
	font = "Angelina",
	size = 48,
	weight = 100,
})