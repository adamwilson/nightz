--[[

	The purpose of this file is just to
	keep all the util.AddNetworkStrings in
	one place, they're messy

]]

util.AddNetworkString("showCharCreationMenu")
util.AddNetworkString("closeCharCreationMenu")
util.AddNetworkString("finishCharCreationMenu")

util.AddNetworkString("updateInventory")
util.AddNetworkString("updateBackpack")
util.AddNetworkString("updateBackpackInitial")
util.AddNetworkString("backpackToInventory")
util.AddNetworkString("backpackToBackpack")
util.AddNetworkString("inventoryToInventory")
util.AddNetworkString("inventoryToBackpack")