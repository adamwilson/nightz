--[[
        Job Arguments:
                bleedingChance          - The chance of the job bleeding when taking damage in percentage
                sprintSpeed             - This is added to the default sprinting speed. Default is 250.
                hungerDecay             - This changes how much hunger is taken away. -0.1 through -1
                thirstDecay             - This changes how much thirst is taken away. -0.1 through -1
]]
 
nightz.jobs = {
        {
                name = "Doctor",
                desc = "doing surgeries and saving lives.",
                bleedingChance = 5
        },
        {
                name = "Police Officer",
                desc = "catching criminals."
        },
        {
                name = "Fire Officer",
                desc = "saving lives."
        },
        {
                name = "Builder",
                desc = "working 9 til 5."
        },
        {
                name = "Chef",
                desc = "working in a 5 star restaurant.",
                hungerDecay = -0.8,
                thirstDecay = -0.8
        },
        {
                name = "Fitness Instructor",
                desc = "living a healthy lifestyle.",
                sprintSpeed = 30
        }
}