--[[

	  _   _ _       _     _   ______   _____             __ _       
	 | \ | (_)     | |   | | |___  /  / ____|           / _(_)      
	 |  \| |_  __ _| |__ | |_   / /  | |     ___  _ __ | |_ _  __ _ 
	 | . ` | |/ _` | '_ \| __| / /   | |    / _ \| '_ \|  _| |/ _` |
	 | |\  | | (_| | | | | |_ / /__  | |___| (_) | | | | | | | (_| |
	 |_| \_|_|\__, |_| |_|\__/_____|  \_____\___/|_| |_|_| |_|\__, |
			   __/ |                                           __/ |
			  |___/                                           |___/ 
			  
]]

--[[
  _____        __            _ _   
 |  __ \      / _|          | | |  
 | |  | | ___| |_ __ _ _   _| | |_ 
 | |  | |/ _ \  _/ _` | | | | | __|
 | |__| |  __/ || (_| | |_| | | |_ 
 |_____/ \___|_| \__,_|\__,_|_|\__|
                                   
]]

--The following variables are the ones inserted
--into the database when a person first joins
defaultConfig = {}
defaultConfig.thirst = 100
defaultConfig.health = 100
defaultConfig.hunger = 100
defaultConfig.status = util.TableToJSON( {
	{ "wet", false },
	{ "sick", false },
	{ "brokenBones", false }
} )
defaultConfig.backpackSize = 16
defaultConfig.backpack = util.TableToJSON( {
	{ 
		{}, {}, { name = "Glock", class = "fas2_glock20", mdl = "models/weapons/w_pist_glock18.mdl", desc = "N/A" }, {} 
	},
	{
		{}, {}, {}, {} 
	},
	{
		{}, {}, {}, {} 
	},
	{
		{}, {}, { name = "AK47", class="fas2_ak47", mdl = "models/weapons/w_rif_ak47.mdl", desc = ""}, {} 
	}
} )
defaultConfig.inventory = util.TableToJSON( 
	{ 
		{},
		{},
		{},
		{},
		{},
		{},
		{}
	}
)

--List of possible playermodels the player can spawn as
playerModels = {
	"models/player/group03/female_01.mdl",
	"models/player/group03/female_02.mdl",
	"models/player/group03/female_03.mdl",
	"models/player/group03/female_04.mdl",
	"models/player/group03/female_05.mdl",
	"models/player/group03/female_06.mdl",
	"models/player/group03/male_01.mdl",
	"models/player/group03/male_02.mdl",
	"models/player/group03/male_03.mdl",
	"models/player/group03/male_04.mdl",
	"models/player/group03/male_05.mdl",
	"models/player/group03/male_06.mdl",
	"models/player/group03/male_07.mdl",
	"models/player/group03/male_08.mdl",
	"models/player/group03/male_09.mdl"
	--Remember the last one shouldn't have a comma
}

--[[
  __  __                                     _   
 |  \/  |                                   | |  
 | \  / | _____   _____ _ __ ___   ___ _ __ | |_ 
 | |\/| |/ _ \ \ / / _ \ '_ ` _ \ / _ \ '_ \| __|
 | |  | | (_) \ V /  __/ | | | | |  __/ | | | |_ 
 |_|  |_|\___/ \_/ \___|_| |_| |_|\___|_| |_|\__|
                                                 
]]

nightz.walkSpeed = 140
nightz.runSpeed = 250

--[[

   _____                            _             
  / ____|                          (_)            
 | (___  _ __   __ ___      ___ __  _ _ __   __ _ 
  \___ \| '_ \ / _` \ \ /\ / / '_ \| | '_ \ / _` |
  ____) | |_) | (_| |\ V  V /| | | | | | | | (_| |
 |_____/| .__/ \__,_| \_/\_/ |_| |_|_|_| |_|\__, |
        | |                                  __/ |
        |_|                                 |___/ 
		
]]

--Angles/positions for the character creation background
nightz.spawnPos = Vector( -3427.409424, -11741.640625, -600.777283 )
nightz.spawnAng = Angle( 2.6, 55.9, 0 )

--Set to true for custom spawn points, false for map spawns
nightz.useCustomSpawns = false
nightz.customSpawns = {
	Vector( 0, 0, 0 ),
	Vector( 0, 0, 0 ),
	Vector( 0, 0, 0 )
}


--[[
  _   _                           
 | \ | |                          
 |  \| | __ _ _ __ ___   ___  ___ 
 | . ` |/ _` | '_ ` _ \ / _ \/ __|
 | |\  | (_| | | | | | |  __/\__ \
 |_| \_|\__,_|_| |_| |_|\___||___/
                                  
]]

nightz.maleFirstNames = {
	"Jacob",
	"Ethan",
	"Andrew",
	"Daniel",
	"Tyler",
	"Adam",
	"Brandon",
	"Kevin",
	"Kyle",
	"Henry",
	"Grant",
	"Shane",
	"Rick",
	"Edward",
	"Colby",
	"Joel",
	"Jack",
	"Ronald",
	"Gabe",
	"Axel"
}
nightz.femaleFirstNames = {
	"Emily",
	"Olivia",
	"Ellie",
	"Ava",
	"Charlotte",
	"Kelly",
	"Daisy",
	"Sophie",
	"Lauren",
	"Erin",
	"Lilly",
	"Emma",
	"Amber",
	"Megan",
	"Summer",
	"Katie",
	"Chloe",
	"Kate",
	"Lucy"
}

nightz.lastNames = {
	"Wilson",
	"Taylor",
	"Brown",
	"Evans",
	"Roberts",
	"White",
	"Harris",
	"Clarke",
	"Lee",
	"Scott",
	"Watson",
	"Parker",
	"Hill",
	"Gray",
	"Palmer",
	"Mills",
	"Dawson"
}

nightz.femaleModels = {
	"models/player/Group03/Female_01.mdl",
	"models/player/Group03/Female_02.mdl",
	"models/player/Group03/Female_03.mdl",
	"models/player/Group03/Female_04.mdl",
	"models/player/Group03/Female_06.mdl"
}
nightz.maleModels = {
	"models/player/group01/male_01.mdl",
	"models/player/Group01/Male_02.mdl",
	"models/player/Group01/male_03.mdl",
	"models/player/Group01/Male_04.mdl",
	"models/player/Group01/Male_05.mdl",
	"models/player/Group01/Male_06.mdl",
	"models/player/Group01/Male_07.mdl",
	"models/player/Group01/Male_08.mdl",
	"models/player/Group01/Male_09.mdl"	
}
