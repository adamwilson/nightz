--[[

Renders the regular world fog

--]]

hook.Add( "SetupWorldFog", "SetupFog", function()
	render.FogMode( 1 )
	render.FogStart( -500 )
	render.FogEnd( 4000  )
	render.FogMaxDensity( 1 )
	render.FogColor( 68, 62, 50 )
	return true
end)

--[[

Renders the fog inside the skybox so that the skybox is consistent and fades off well

--]]

hook.Add( "SetupSkyboxFog", "SetupFog", function( skyboxscale )
	render.FogMode( 1 )
	render.FogStart( -500 * skyboxscale )
	render.FogEnd( 4000 * skyboxscale  )
	render.FogMaxDensity( 1 )
	render.FogColor( 68, 62, 50 )
	return true
end)