--[[

	The purpose of this file is to set-up
	a system so flash-lights can be found/used
	rather than the player just always having one
	like in HL2

]]

--Disable the HL2 flash-light, we want our own system
function GM:PlayerSwitchFlashlight( ply, bool )
	--We need the IsValid check because disconnecting without
	--turning off your flashlight seems to call this hook
	--with an invalid player object since the player doesn't exist
	
	if IsValid( ply:GetActiveWeapon() ) then
		local x, y, tr = ply:hasEntInBackpack("nightz_flashlight")
		if not tr then pos, tr = ply:hasEntInInventory("nightz_flashlight") end
		
		return tr
	end
end