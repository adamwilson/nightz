/*

	This file handles the spawning of the loot across
	the map, it's done dynamically based on the props
	in the map.

*/

local loot = {}
loot.items = {
}
loot.items.drinks = {
}
loot.items.ammo = { 
	"cw_ammo_44magnum", 
	"cw_ammo_50ae", 
	"cw_ammo_545x39", 
	"cw_ammo_556x45", 
	"cw_ammo_762x51", 
	"cw_ammo_9x19"
}
loot.items.weapons = {

}

--Function: 
--	SpawnAmmo( x, y, z )
--Purpose:
--	Helper function to save repeating code for spawning entities
--	by spawning a random loot item.
function loot:spawnLoot(x, y, z)
	local spawnedLoot = ents.Create(table.Random(loot.items.ammo))
	spawnedLoot:SetPos(Vector(x, y, z))
	spawnedLoot:Spawn()
end

--Function:
--	spawn()
--Purpose:
--	Spawns loot around prop_physics entities
function loot:spawn()
	 for k,v in pairs( ents.FindByClass( "prop_physics" ) ) do
		local PosX = v:GetPos().x
		local PosY = v:GetPos().y
		local PosZ = v:GetPos().z + 20 -- For some reason this makes it have a 'airdrop' effect. Keep it this way.

		loot.spawnLoot(PosX, PosY, PosZ)
		for k, y in pairs(ents.FindByClass( 'cw_*' )) do
			Report("Spawned Ammo -> "..y:GetClass())
		end
	end
end

--Debug/Admin command to test loot spawning
concommand.Add("admin_spawn_loot", function(ply)
	loot:spawn()
end)

