--[[

	The purpose of this file is to house the functions
	that allow the adding and modifiying of the player's clothing

]]

local ply = FindMetaTable("Player")

--This is the list of items that are accepted
--as inputs.
nightz.clothingTypes = {
	--Slot Name = { "bone name", "attachment index" }
	head = { "ValveBiped.Bip01_Head1", "eyes" },
	body = { "ValveBiped.Bip01_Spine", "chest" },
	legs = true
}

--Function:
--	addClothing
--Purpose:
--	Adds an item of clothing to the player's body
--Arguments:
--	Name of the item
--	Where to put the item (head, body, legs)
--	The model of the item
function ply:addClothing( item, pos, mdl )
	self.clothing = {
		head = { "", "name" },
		body = { "", "name" },
		legs = { "", "name" }
	}
	
	--Make sure what they've entered is actually a slot
	if not nightz.clothingTypes[pos] then return end
	
	--Create and parent the object
	local obj = ents.Create("prop_dynamic")
	obj:SetModel( mdl )
	obj.Owner = self
	
	local attach = self:LookupAttachment( nightz.clothingTypes[pos][2] )	
	local attachPos = self:GetAttachment( attach )
	
	local bone = self:LookupBone( nightz.clothingTypes[pos][1] )
	local bonePos, boneAng = self:GetBonePosition( attach )
	
	if nightz.clothingTypes[pos] == "body" then
		bonePos = bonePos - Vector( 0, 0, 13 )
	end
	
	obj:SetPos( bonePos )
	obj:SetAngles( attachPos["Ang"] )
	
	obj:SetParent( self, attach )
	obj:SetMoveType( MOVETYPE_NONE )
	obj:Spawn()
	obj:SetCollisionGroup(COLLISION_GROUP_WEAPON)
	
	self.clothing[pos] = { 
		ent = obj, 
		name = item
	}
end

function ply:removeClothing( pos )
	--First check if it exists
	if self.clothing[pos] then
		--Remove the piece of clothing
		self.clothing[pos]["ent"]:Remove()
		--Empty out the clothing slot
		self.clothing[pos] = {}
	end
end