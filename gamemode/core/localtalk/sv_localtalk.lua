/*

	This module checks to see if the player is near
	another player, if so then they'll be able to
	hear each other.

*/

--Max distance the players can be before they stop hearing each other
local maximumDistance = 200
--Should we enable 3D/Surround sound?
local surroundSound = true

function GM:PlayerCanHearPlayersVoice( listener, talker )
	if listener:GetPos():Distance( talker:GetPos() ) > maximumDistance then
		return true, surroundSound
	end
end