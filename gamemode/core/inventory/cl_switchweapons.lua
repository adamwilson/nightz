--[[

	The switch weapons system works as follows:
	1.) The player presses 1 to get a GLOCK from their hotbar
	2.) The client checks what's in that slots and assigns the variable nightzShouldSwitchWeapon to the weapon entity
	3.) The CreateMove hook handles the weapon switching which runs clientside

	If you're looking for the code that disables the HL2
	weapon selection, it's inside the 'hud' module
]]

--I feel the best way of checking if the player is pressing
--the 1-7 is to check what they've got assigned to the bind
--slot1 all the way through to slot7

local slots = {
	"slot1",
	"slot2",
	"slot3",
	"slot4",
	"slot5",
	"slot6",
	"slot7"
}

function GM:PlayerBindPress( ply, bind, pressed )
	if table.HasValue( slots, bind ) then
	
		--Find out the numeric value (1-7)
		local pos
		for k,v in pairs( slots ) do
			if v == bind then
				pos = k
			end
		end
		
		--Find the weapon
		local wep = nightz.inventory[pos]["class"]
		
		--When we set nightzShouldSwitch weapon to the entity
		--of the weapon we want to switch to, the CreateMove hook
		--below will automatically take care of it.
		for k,v in pairs( LocalPlayer():GetWeapons() ) do
			if v:GetClass() == wep then
				nightzShouldSwitchWeapon = v
			end
		end
		
		weaponIsSwitching = true
		
		timer.Simple( 2, function()
			weaponIsSwitching = false
		end )
	end
end

--This is done inside CreateMove because the prediction
--works correctly when called clientside, if you try it
--serverside it won't run any deploy/holster commands
hook.Add( "CreateMove", "SwitchWeaponCreateMove", function( cmd )
	if nightzShouldSwitchWeapon then	
		cmd:SelectWeapon( nightzShouldSwitchWeapon )
		nightzShouldSwitchWeapon = nil
	end
end)