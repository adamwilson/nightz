--[[

	The purpose of this file is to check and save
	the amount of ammo that the player has when they
	switch weapons.
	
	This means that the player can't just get infinite
	ammo from dragging the weapon into their inventory
	and then back out into their hotbar

]]

hook.Add("PlayerSwitchWeapon", "switchWeaponAmmoCheck", function( ply, old, new )
	--Make sure they have the correct ammo
	if IsValid( new ) then
		for k,v in pairs( ply.inventory ) do
			if v["class"] == new:GetClass() and v["ammo1"] and v["ammo2"] then
				if new:Clip1() != v["ammo1"] then
					new:SetClip1( v["ammo1"] )
				end
				
				--This is because FAS2 handles reserve ammo differently
				--So set the secondary ammo using this method
				if new.Primary then
					if new.Primary.Ammo then
						ply:SetAmmo( v["ammo2"], new.Primary.Ammo )
					end
				else
					--This is how regular weapons handle it
					if new:Clip2() != v["ammo2"] then
						new:SetClip2( v["ammo"] )
					end
				end
			end
		end
	end

	if not IsValid( old ) or not IsValid( new ) then return end
	
	--For some reason FAS2 weapons have their Clip2 differently
	--In order to get the reserve ammo for FAS2 weapons you need
	--to do ply:GetAmmoAccount( weaponEntity.Primary.Ammo )
	
	local oldClip = old:Clip1()
	local oldRes = old:Clip2()
	
	if old.Primary then
		oldRes = ply:GetAmmoCount( old.Primary.Ammo )
	end
	
	--Find which slot the old weapon was in and set
	--the amount of ammo
	for k,v in pairs( ply.inventory ) do
		if v["class"] == old:GetClass() then
			v["ammo1"] = oldClip
			v["ammo2"] = oldRes
		end
	end
end)