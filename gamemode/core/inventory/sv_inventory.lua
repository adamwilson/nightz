--[[

	The purpose of this file is to handle the
	hotbar system.

]]

--Find and store the meta-table for later use
local ply = FindMetaTable("Player")

--Function: 
--	updateInventory()
--Purpose:
--	Gets the server-side inventory and updates
--	the client with it
function ply:updateInventory()
	net.Start("updateInventory")
		net.WriteTable( self.inventory )
	net.Send( self )
end

--Function:
--	addToInventory()
--Purpose:
--	Adds an item to the player's inventory
--Arguments:
-- A table containing:
--	tbl.name	The name of the item
--	tbl.desc	Small description
--	tbl.mdl		Model path
--	tbl.class	The actual entity
--Position of the item in the inventory
function ply:addToInventory( item, pos )
	--Inventory limit is 7 so make sure it can't be longer
	if pos > 7 then return end
	
	if item.name and item.desc and item.mdl and item.class then
		self.inventory[pos] = item
		self:updateInventory()
	end
end

net.Receive("inventoryToInventory", function( l, ply )
	local oldPos = net.ReadInt( 6 )
	local newPos = net.ReadInt( 6 )
	
	--Make sure we've got both positions
	if not oldPos or not newPos then return end
	
	local old = ply.inventory[oldPos]
	local new = ply.inventory[newPos]
	
	--If both exist then swap them
	--We have to check for class because doing just new/old
	--will return true since they're just empty tables when
	--there's no items in them
	--
	--if new["class"] and old["class"] then
	--	ply.inventory[newPos] = old
	--	ply.inventory[oldPos] = new
	--end
	--
	--This code is for when the clientside functionality of
	--swapping items has been sorted
	
	--If we're moving something to an empty slot
	if old["class"] and not new["class"] then
		ply.inventory[newPos] = old
		ply.inventory[oldPos] = {}
	end
end)

net.Receive("backpackToInventory", function( l, ply )
	local item = net.ReadTable()
	local vert = net.ReadInt( 10 )
	local horiz = net.ReadInt( 10 )
	local pos = net.ReadInt( 4 )
	
	--Don't let the player override anything that's already there
	if ply.inventory[pos]["class"] then return end

	--Make sure the player actually has the item they claim they have
	if not ply:hasEntInBackpack( item.class ) then return end
	
	if not ply.backpack[vert][horiz] == item then return end
	
	ply:addToInventory( item, pos )
	ply.backpack[vert][horiz] = {}
	ply:Give( item.class )
	
	ply:updateBackpack()
end)

--Function:
--	hasEntInInventory
--Purpose:
--	Checks whether or not the player has an item in their inventory
--Arguments:
--	The class of the item to check
function ply:hasEntInInventory( item )
	for k,v in pairs( self.inventory ) do
		if v["class"] == item then 
			return k, true
		end
	end
end