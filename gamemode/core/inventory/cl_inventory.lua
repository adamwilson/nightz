--[[

	The purpose of this file is to handle the
	hotbar system.

]]

--These menu colours are purely temporary in order to
--get functionality down, I know there's duplicates but
--it'll shortly be moved into nightz.colours
local menuColors = {
	Black 		= Color(0, 0, 0, 255),
	TBlack 		= Color(50, 50, 50, 200),
	TBlackBG	= Color(25, 25, 25, 200),
	Grey 		= Color(100, 100, 100, 255),
	TGrey		= Color(100, 100, 100, 200 ),
	Green 		= Color(39, 174, 96, 255 ),
	TGreen 		= Color(39, 174, 96, 20 ),
}

local startTime = 0
local lifeTime = 0
local startVal = 0
local endVal = 0
local value = 0

function hideHotbar()
	hotbarIsHidden = true
	hotbarFrame:SetVisible( false )
	
	startTime = 0
	lifeTime = 0.15
	startVal = nightz.colours["DarkGreyAlpha"].a
	endVal = 0
	value = startVal
	startTime = CurTime()
end

function showHotbar()
	hotbarIsHidden = false
	hotbarFrame:SetVisible( true )
	
	startTime = 0
	lifeTime = 0.15
	startVal = 0
	endVal = nightz.colours["DarkGreyAlpha"].a
	value = startVal
	startTime = CurTime()
end

function isHotbarHidden()
	return hotbarIsHidden
end

--Start disabled
hotbarIsHidden = true

hook.Add("HUDPaint", "hotbarHUD", function()
	local fraction = ( CurTime( ) - startTime ) / lifeTime
	fraction = math.Clamp( fraction, 0, 1 )
	value = Lerp( fraction, startVal, endVal )
	draw.RoundedBox( 0, (ScrW()/2)-247.5, ScrH()-90, 495, 75, Color( nightz.colours["GreyAlpha"].r, nightz.colours["GreyAlpha"].g, nightz.colours["GreyAlpha"].b, value ) )
	
	for i=1,7 do
		draw.RoundedBox( 0, (ScrW()/2)-(70*i)+247.5, ScrH()-85, 65, 65, Color( nightz.colours["DarkGreyAlpha"].r, nightz.colours["DarkGreyAlpha"].g, nightz.colours["DarkGreyAlpha"].b, value ) )
		draw.SimpleText( i, "Default", (ScrW()/2)+(70*i)-300, ScrH()-75, Color( nightz.colours["Grey"].r, nightz.colours["Grey"].g, nightz.colours["Grey"].b, value ), 1, 1 )
	end
end )

local function createHotbarModels()
	hotbarFrame = vgui.Create("DFrame")
	hotbarFrame:SetPos( (ScrW()/2)-250, ScrH()-90)
	hotbarFrame:SetSize(495, 75)
	hotbarFrame:ShowCloseButton( false )
	hotbarFrame:SetTitle("")
	hotbarFrame:SetVisible( false )
	hotbarFrame.Paint = function( s ) end
	
	hotbarInventory = vgui.Create("DPanelSelect", hotbarFrame)
	hotbarInventory:SetPos( 6, 3 )
	hotbarInventory:SetSpacing( 6 )
	hotbarInventory:SetPadding( 2 )	
	hotbarInventory:SetSize( hotbarFrame:GetWide(), hotbarFrame:GetTall() )
	
	for i=1,7 do
		hotbarSlot = vgui.Create("DPanelSelect", hotbarInventory)
		hotbarSlot:SetSize( 64, 68 )
		hotbarSlot:SetSpacing( 0 )
		hotbarSlot:SetPadding( 0 )
		hotbarSlot:Receiver( "slot", function( pnl, tbl, dropped, menu, x, y )
			if ( !dropped ) then return end
			
			local action
			local curItems = pnl:GetItems()
			--Limit the amount of items to 1 per slot
			if #curItems >= 1 then return end
			
			--Lets check to see if the item is coming from the backpack
			--If it is then we need the position
			
			local vert, horiz
			for k,v in pairs( backpack:GetItems() ) do
				for key, val in pairs( v:GetItems() ) do
					--We need to go deeper, surely there's a better way of doing this?
					for keys, values in pairs( val:GetItems() ) do
						if values == tbl[1] then
							vert, horiz = k, key
						end
					end
				end
			end
			
			--If it's from the backpack, then move from the
			--backpack to the inventory
			if vert and horiz then 
				action = true
				local pos = i
				backpackToInventory( tbl[1], vert, horiz, pos )				
			end
			
			--Lets check if the player is simply trying to move
			--an item from the inventory to a different slot
			local oldSlot, newSlot
			for k,v in pairs( hotbarInventory:GetItems() ) do
				if v == pnl then
					newSlot = k
				end
			
				for key, val in pairs( v:GetItems() ) do
					if val == tbl[1] then
						oldSlot = k
					end
				end
			end
			
			--If it's being moved from an old slot to a
			--new slot then run the
			if oldSlot and newSlot then
				action = true
				inventoryToInventory( oldSlot, newSlot )
			end
			
			--We want to make sure that there's an action
			--taken place, if not then don't move the item
			if action then
				tbl[1]:SetSize( 64, 64 )
				pnl:AddItem(tbl[1])
			end
		end )
		
		hotbarInventory:AddItem( hotbarSlot )
	end
end
createHotbarModels()

--Function:
--	inventoryToInventory
--Purpose:
--	Used for when an item is moved from one slot to
--	another within the inventory
--Arguments:
--	The slot to move the item from
--	The slot to move the item to
function inventoryToInventory( old, new )
	net.Start("inventoryToInventory")
		net.WriteInt( old, 6 )
		net.WriteInt( new, 6 )
	net.SendToServer()
end

--Function:
--	backpackToInventory
--Purpose:
--	Swap something from the backpack to the inventory
function backpackToInventory( item, vert, horiz, pos )
	if item.name and item.mdl and item.class and item.desc then
	
		--We could send the whole thing but why not
		--just sent the essentials, the server doesn't need
		--to know that it's a DModelPanel
		local swapItem = {}
		swapItem.name = item.name
		swapItem.mdl = item.mdl
		swapItem.class = item.class
		swapItem.desc = item.desc
	
		net.Start("backpackToInventory")
			net.WriteTable( swapItem )
			--Using a second param of 10 bits means we're limited to
			--backpacks of 1024 and less, this shouldn't be an issue
			--For reference: http://www.exploringbinary.com/a-table-of-nonnegative-powers-of-two/
			net.WriteInt( vert, 10 )
			net.WriteInt( horiz, 10 )
			net.WriteInt( pos, 4 )
		net.SendToServer()
	end
end

--Function: 
--	addToHotbar
--Purpose:
--	Adds an item to the hotbar (clientside/visually)
--Arguments:
-- A table containing:
--	tbl.name	The name of the item
--	tbl.desc	Small description
--	tbl.mdl		Model path
--	tbl.class	The entity name
-- The slot to put the item in
function addToInventory( item, slot )
	--Make sure what we're adding is actually a table
	if item.name and item.desc and item.mdl and item.class then
		local newItem = vgui.Create("DModelPanel")
		newItem:SetSize(64,64)
		newItem:Droppable("slot")
		newItem:SetModel( item.mdl )
		
		--For later reference
		newItem.name = item.name
		newItem.class = item.class
		newItem.mdl = item.mdl
		newItem.desc = item.desc
		
		--Thanks to Giraffen93
		--http://facepunch.com/showthread.php?t=1443651&p=46806108&viewfull=1#post46806108
		local pnl = baseclass.Get("DModelPanel")
		newItem.Paint = function(self,w,h)
			surface.SetDrawColor( menuColors.TGreen )
			surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() )
			pnl.Paint(self, w, h)
		end
		
		--Disable the spinning of the model
		function newItem:LayoutEntity( ent ) end
		
		GenerateView( newItem )
		
		local hotbarSlot = hotbarInventory:GetItems()[slot]
		hotbarSlot:AddItem( newItem )
	end
end

--Used whenever the player's server-side inventory changes
net.Receive("updateInventory", function()
	nightz.inventory = net.ReadTable()
	
	for k,v in pairs( hotbarInventory:GetItems() ) do
		v:Clear()
	end
	
	for k,v in pairs( nightz.inventory ) do
		addToInventory( v, k )
	end
end)

--Essentially if the key G is held down then show the hotbar
--This is done using the result of the booleans of if the
--key is down and if the hotbar is hidden.
hook.Add( "Tick", "shouldDrawHotbar", function()
	--Prevent the UI showing whilst chat or console is open
	if gui.IsConsoleVisible() or gui.IsGameUIVisible() or LocalPlayer():IsTyping() then return end
	
	if input.IsKeyDown( KEY_G ) and hotbarIsHidden then
		showHotbar()
		showBackpack()
	elseif not input.IsKeyDown( KEY_G ) and not hotbarIsHidden and not weaponIsSwitching then
		hideHotbar()
		hideBackpack()
	elseif weaponIsSwitching and hotbarIsHidden then
		showHotbar()
	end
end )