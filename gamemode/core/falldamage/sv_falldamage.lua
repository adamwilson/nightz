/*
	
	By default Garry's Mod does 10 damage
	per time you fall, this is pretty unrealistic
	so this module aims to fix that.

*/
--This takes the player's falling speed and
--divides it by 8 to give a realistic damage number
function GM:GetFallDamage( ply, speed )
	 return ( speed / 16 )
end