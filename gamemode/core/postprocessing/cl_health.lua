--[[

This file handles the post processing
when the player's health drops below
a certain amount

--]]

local function CheckHealth()
	local tab = {}
	tab[ "$pp_colour_addr" ] = 0
	tab[ "$pp_colour_addg" ] = 0
	tab[ "$pp_colour_addb" ] = 0
	tab[ "$pp_colour_brightness" ] = 0
	tab[ "$pp_colour_contrast" ] = 1
	-- Convert the player's health to a value between 0.0 and 1.0 by
	-- dividing it by 100. So 50 health would give us 0.5 (50% color)
	tab[ "$pp_colour_colour" ] = LocalPlayer():Health() / 100
	tab[ "$pp_colour_mulr" ] = 0
	tab[ "$pp_colour_mulg" ] = 1
	tab[ "$pp_colour_mulb" ] = 1 
	
	DrawColorModify( tab )
end
hook.Add( "RenderScreenspaceEffects", "RenderColorModify", CheckHealth )