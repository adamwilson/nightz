--[[

Draws the Vignette effect on the player

--]]

function DrawVignette()
	DrawMaterialOverlay( "overlays/vignette01", 1 )
end
hook.Add( "RenderScreenspaceEffects", "Vignette_Overlay", DrawVignette )