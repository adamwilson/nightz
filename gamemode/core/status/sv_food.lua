local food = {}
food.path = "models/warz/consumables/"
local randomFood = {
	food.path.."bag_chips.mdl",
	food.path.."bag_mre.mdl",
	food.path.."bag_oat.mdl",
	food.path.."bar_chocolate.mdl",
	food.path.."bar_granola.mdl",
	food.path.."can_pasta.mdl",
	food.path.."can_soup.mdl",
	food.path.."can_spam.mdl",
	food.path.."can_tuna.mdl"
}
food.spawnPoints = {
	Vector( 14109.424805, 8449.501953, -7487.968750 ),
	Vector( 13774.307617, 9380.880859, -7479.968750 ),
	Vector( 11405.398438, 5326.070313, -7476.804199 ),
	Vector( 7295.694336, 5941.772949, -9895.555664 ),
	Vector( 5323.254395, 10169.766602, -10329.822266 ),
	Vector( -1217.540771, 6140.349121, -10076.274414 ),
	Vector( -9430.843750, 5414.887207, -10015.968750 ),
	Vector( -13108.346680, 10510.651367, -10023.968750 ),
	Vector( -11955.382813, 10541.202148, -10015.968750 ),
	Vector( -11033.373047, 10651.084961, -10008.968750 ),
	Vector( -10837.982422, 9790.500000, -10023.968750 ),
	Vector( -10233.051758, 9582.971680, -10023.968750 ),
	Vector( -9836.150391, 9222.060547, -10008.968750 ),


}

function food:forceSpawn()
	for k, v in pairs(food.spawnPoints) do
		local food = ents.Create( "ent_food" )
		food:SetModel(table.Random(randomFood))
		food:SetPos( Vector(v) )
		food:Spawn()
		
	end
end

timer.Simple(math.random(300, 600), function()
	food.forceSpawn()
	Report("Spawning food.")
end)

local ply = FindMetaTable("Player")
function ply:eat( food )
	if IsValid( food ) and IsEntity( food ) then
		if (self:getHunger() < 100) then
			self:setHunger( math.random(5, 10) )
			food:Remove()
		end	
	end
end