local drinks = {}
drinks.path = "models/warz/consumables/"
local randomFood = {
	drinks.path.."water_s.mdl",
	drinks.path.."water_l.mdl",
	drinks.path.."soda.mdl",
	drinks.path.."juice.mdl",
	drinks.path.."gatorade.mdl",
	drinks.path.."energy_drink.mdl",
	drinks.path.."coconut_water.mdl"
}

drinks.spawnPoints = {
	Vector( 14109.424805, 8449.501953, -7487.968750 ),
	Vector( 13774.307617, 9380.880859, -7479.968750 ),
	Vector( 11405.398438, 5326.070313, -7476.804199 ),
	Vector( 7295.694336, 5941.772949, -9895.555664 ),
	Vector( 5323.254395, 10169.766602, -10329.822266 ),
	Vector( -1217.540771, 6140.349121, -10076.274414 ),
	Vector( -9430.843750, 5414.887207, -10015.968750 ),
	Vector( -13108.346680, 10510.651367, -10023.968750 ),
	Vector( -11955.382813, 10541.202148, -10015.968750 ),
	Vector( -11033.373047, 10651.084961, -10008.968750 ),
	Vector( -10837.982422, 9790.500000, -10023.968750 ),
	Vector( -10233.051758, 9582.971680, -10023.968750 ),
	Vector( -9836.150391, 9222.060547, -10008.968750 ),


}

function drinks:forceSpawn()
	for k, v in pairs(drinks.spawnPoints) do
		local drink = ents.Create( "ent_drink" )
		drink:SetModel(table.Random(randomFood))
		drink:SetPos( Vector(v) )
		drink:Spawn()
	end
end

timer.Simple(math.random(300, 600), function()
	drinks.forceSpawn()
	Report("Spawning drinks.")
end)

local ply = FindMetaTable("Player")
function ply:drink( drink )
	if IsValid( drink ) and IsEntity( drink ) then
		if (self:getThirst() < 100) then
			self:setThirst( math.random(10, 35) )
			drink:Remove()
		end	
	end
end