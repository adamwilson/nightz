/*

	This file handles all the setting
	of the status effects

*/

--Store the player metatable to reference later on
local ply = FindMetaTable( "Player" )

--The max and minimum amount of something we should have
--All health/hunger/thirst is clamped between these
local clampMin = 0
local clampMax = 100
local decayAmount = -1
local sprintMultiplier = 2

--Function:
--	setHunger
--Purpose:
--	Sets the hunger level of the player,
--	anything outside of 0/100 is clamped
function ply:setHunger( amount )
	local amount = math.Clamp( self:getHunger() + amount, clampMin, clampMax )
	self:SetNWInt( "hunger", amount )
end

--Function:
--	setThirst
--Purpose:
--	Sets the thirst level of the player,
--	anything outside of 0/100 is clamped
function ply:setThirst( amount )
	local amount = math.Clamp( self:getThirst() + amount, clampMin, clampMax )
	self:SetNWInt( "thirst", amount )
end

--Function:
--	setHealth
--Purpose:
--	Not necessary but keeps it consistent,
--	also clamped
function ply:setHealth( amount )
	local amount = math.Clamp( self:getHealth() + amount, clampMin, clampMax )
	self:SetHealth( amount )
end

--Function:
--	setWet
--Purpose:
--	Sets the status of 'wet' to true
function ply:setWet()
	self:SetNWBool("wet", true)
end

--Function:
--	setDry
--Purpose:
--	Sets the status of 'wet' to false
function ply:setDry()
	self:SetNWBool("wet", false )
end

--Function:
--	setBrokenBones
--Purpose:
--	Sets the player to have broken bones
function ply:setBrokenBones()
	self:SetNWBool("brokenBones", true )
end

--Function:
--	fixBrokenBones
--Purpose:
--	Disables the effect of broken bones
function ply:fixBrokenBones()
	self:SetNWBool("brokenBones", false )
end

--Function:
--	makeSick
--Purpose:
--	Sets the player status sick to 'true'
function ply:makeSick()
	self:SetNWBool("sick", true )
end

--Function:
--	disableSick
--Purpose:
--	Disable the effects of the status 'sick'
function ply:disableSick()
	self:SetNWBool("sick", false )
end