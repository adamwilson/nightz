--[[

	The purpose of this file is to make sure
	the player's hunger decays over time

	]]

function TakeHunger(ply)
	if not IsValid(ply) then return end

	-- If the hunger is gone don't do anything
	if ply:getHunger() < 0 then return end

	local takeHunger = -1
	local takeThirst = -1
	
	if (ply:KeyDown(IN_SPEED)) then
		
		--Double the value if they're sprinting
		takeHunger, takeThirst = -2
		if ply.job.hungerDecay then
			takeHunger = ply.job.hungerDecay * -2
		end
		
		if ply.job.thirstDecay then
			takeThirst = ply.job.thirstDecay * -2
		end
	else
		if ply.job.hungerDecay then
			takeHunger = ply.job.hungerDecay * -1
		end
		
		if ply.job.thirstDecay then
			takeThirst = ply.job.thirstDecay * -1
		end
	end 
	
	ply:setHunger(takeHunger)
	ply:setThirst(takeThirst)
end
hook.Add("NightZ_TakeHunger", TakeHunger)

function TakeHungerThink()
	timer.Create("NightZ_TakeHungerTimer", math.random(15, 25), 0, function()
		for k, v in pairs(player.GetAll()) do
			if not IsValid(v) then return end

			if (math.random(1, 2) == 1) then
				if not v.inCharCreation then
					TakeHunger(v)
				end
			end	
		end

		if (timer.Exists("NightZ_TakeHungerTimer")) then
			timer.Destroy("NightZ_TakeHungerTimer")
		end
		TakeHungerThink()
	end)
end

TakeHungerThink()