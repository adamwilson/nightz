/*

	This file has a bunch of helper functions
	to assist in getting information about the
	player such as their health, hunger, thirst, etc

*/

-- Find the meta table so we can reference it later
local ply = FindMetaTable( "Player" )

--Name: 
--	getHealth
--Purpose:
--	To get the player's current health
--	basically the original function renamed
--	for consistency
--Returns:
--	The player's health
function ply:getHealth()
	return self:Health()
end

--Name: 
--	getHunger
--Purpose:
--	To get the player's current hunger
--Returns:
--	The player's hunger
function ply:getHunger()
	return self:GetNWInt("hunger")
end

--Name: 
--	getThirst
--Purpose:
--	To get the player's current thirst
--Returns:
--	The player's thirst
function ply:getThirst()
	return self:GetNWInt("thirst")
end

--Name:
--	getWet
--Function:
--	Checks if the player is currently wet
--Returns:
--	Boolean as to whether the player is wet or not
function ply:getWet()
	return self:GetNWBool("wet")
end

--Name:
--	getSick
--Function:
--	Checks if the player is currently sick
--Returns:
--	Boolean as to whether the player is sick or not
function ply:getSick()
	return self:GetNWBool("sick")
end

--Name:
--	getBrokenBones
--Function:
--	Checks if the player currently has broken bones
--Returns:
--	Boolean as to whether the player has broken bones
function ply:getBrokenBones()
	return self:GetNWBool("brokenBones")
end