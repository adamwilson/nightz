--[[

Plays a custom sound everytime the player
is means to hear a footstep.

--]]

local Footsteps = {
	"npigamers/gear1.wav",
	"npigamers/gear2.wav",
	"npigamers/gear3.wav",
	"npigamers/gear4.wav",
	"npigamers/gear5.wav",
	"npigamers/gear6.wav",
	"npigamers/boots1.wav",
	"npigamers/boots2.wav",
	"npigamers/boots3.wav",
	"npigamers/boots4.wav"
}

function RandomSound() -- Need to have this as a function else a random sound can't be selected each time
	RanFootsteps = table.Random( Footsteps )
end


function GM:PlayerFootstep( ply, pos, foot, sound, volume, rf ) 
	RandomSound()
	local GetSpeed = ply:GetVelocity():Length()

	if ply:Crouching() and GetSpeed > 80 or GetSpeed < 100 then -- Crouching
		ply:EmitSound( RanFootsteps, 25 )
	elseif GetSpeed < 175 and GetSpeed > 140 then 				-- Walking normally
		ply:EmitSound( RanFootsteps, 40 )
	elseif GetSpeed < 140 then 					  				-- Walking/Crouchwalking
		ply:EmitSound( RanFootsteps, 20 )		
	else													 	-- Running
		ply:EmitSound( RanFootsteps, 50 )
	end
	return true
 end