local menuColors = {
	Green = Color(39, 174, 96, 255),
	White = Color(255, 255, 255),
	Grey = Color(199, 205, 209)
}

local menuText = {
	welcomeText = [[Welcome to NightZ, the only DayZ remake that doesn't save your inventory every second!
	
If you are new and don't know how to play, please go to the second tab. Want to know about the community and or what goes on, go the to the FAQ tab!]],

	faqText = [[Here are some questions that are asked a lot:
		
Q: Who created this gamemode?
A: Adzter, the original creator, started this project on 2/8/2015. 
44_oz joined that same day as a developer.
		
Q: Can I download this gamemode/host it?
A: Yes and no, you are allowed to host it if you have a license key from 
one of the original creators.
]],

	h2pText = [[New to the gamemode and want to learn how to play? Here is a short 
little tutorial on how to stay alive:

PLAYER vs. PLAYER vs. ENVIROMENT - This gamemode is heavily survival. 
There is little to no ammo and other players. Although there are melee 
weapons, well, these will not help you at long range.

PLAYER vs. ENVIROMENT - You will have to mange the following for your 
character: stamina, ammo, food, health, and thirst. You will soon come to find
that all of this is hard to keep up with. Drinking and eating will increase 
stamina but only for a short time.]]
}

local function welcomeMenu()
	local mm = vgui.Create("DFrame")
	mm:SetSize(400, 350)
	mm:SetTitle("Welcome Menu")
	mm:SetVisible(true)
	mm:ShowCloseButton(true)
	mm:Center()
	mm:MakePopup()
	mm.Paint = function(s)
		draw.RoundedBox(0, 0, 0, s:GetWide(), s:GetTall(), menuColors.Grey)
		draw.RoundedBox(0, 0, 0, s:GetWide(), 25, menuColors.Green)

	end
	
	local ps = vgui.Create("DPropertySheet", mm)
	ps:SetPos(5, 30)
	ps:SetSize(mm:GetWide() - 10, mm:GetTall() - 35)
	
	local tab1 = vgui.Create("DLabel", mm)
	tab1:SetPos(0,0)
	tab1:SetText("")
	
	local tab2 = vgui.Create("DLabel", mm)
	tab2:SetPos(0,0)
	tab2:SetText("")
	
	local tab3 = vgui.Create("DLabel", mm)
	tab3:SetPos(0,0)
	tab3:SetText("")
	
	// Welcome text
	local txt = vgui.Create( "RichText", tab1 )
	txt:Dock( FILL )
	
	txt:InsertColorChange( 255, 255, 255, 255 )
	txt:AppendText( menuText.welcomeText )
	
	// h2p text
	local txt1 = vgui.Create( "RichText", tab2 )
	txt1:Dock( FILL )
	
	txt1:InsertColorChange( 255, 255, 255, 255 )
	txt1:AppendText( menuText.h2pText )
	
	// faq text
	local txt2 = vgui.Create( "RichText", tab3 )
	txt2:Dock( FILL )
	
	txt2:InsertColorChange( 255, 255, 255, 255 )
	txt2:AppendText( menuText.faqText )
	
	
	
	
	
	
	
	
	
	
	
	
	ps:AddSheet("Welcome", tab1, nil, false, false, "Welcome to the server");
	ps:AddSheet("How to play", tab2, nil, false, false, "How to play the gamemode");
	ps:AddSheet("FAQ", tab3, nil, false, false, "FAQ");
end
concommand.Add("open_welcome_menu", welcomeMenu)