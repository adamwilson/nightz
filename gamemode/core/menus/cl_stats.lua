local function statsMenu()
	local mm = vgui.Create("DFrame")
	mm:SetSize(750, 450)
	mm:SetTitle("")
	mm:SetVisible(true)
	mm:ShowCloseButton(false)
	mm:Center()
	mm:MakePopup()
	mm.Paint = function()
	end
	
	local bg = vgui.Create("DImage", mm )
	bg:SetSize( 750, 450 )
	bg:SetImage( "menu/stats_bg.png" )

	local icon = vgui.Create( "DModelPanel", mm )
	icon:SetModel( LocalPlayer():GetModel() )
	icon:SetPos(78, 47)
	icon:SetSize( 200, 250 )
	
	local eyepos = icon.Entity:GetBonePosition( icon.Entity:LookupBone( "ValveBiped.Bip01_Head1" ) )
	eyepos:Add( Vector( 0, 0, 2 ) )	-- Move up slightly
	
	icon:SetLookAt( eyepos )
	icon:SetCamPos( eyepos - Vector( -23, 0, 0 ) )	-- Move cam in front of eyes
	icon.Entity:SetEyeTarget( eyepos-Vector( -12, 0, 0 ) )
	function icon:LayoutEntity( ent ) return end
	
	local text = vgui.Create("DPanel", mm )
	text:SetSize( mm:GetWide(), mm:GetTall() )
	text.Paint = function()
		draw.SimpleText("Name: "..LocalPlayer():GetName(), "MenuFont", 400, 80, nightz.colours["DarkGrey"] )
		draw.SimpleText("Kills: "..LocalPlayer():Frags(), "MenuFont", 400, 140, nightz.colours["DarkGrey"] )
		draw.SimpleText("Deaths: "..LocalPlayer():Deaths(), "MenuFont", 400, 200, nightz.colours["DarkGrey"] )
	end
	
	local closeButton = vgui.Create( "DButton", mm )
	closeButton:SetSize( 50, 30 )
	closeButton:SetPos( mm:GetWide() - 145, 23 )
	closeButton:SetText("")
	closeButton.DoClick = function()
		mm:Remove()
	end
	closeButton.Paint = function()
		draw.SimpleText("Close", "closeButton", 0, 0, nightz.colours["DarkGrey"] )
	end
end
concommand.Add("open_stats", statsMenu)