--[[

	This file handles the inventory and the backpack
	system of the gamemode, also includes various
	functions to check if the player can hold certain
	items, etc
	
]]

--Find the player meta table so we can reference it
local ply = FindMetaTable( "Player" )

--Function:
--	pickupItem
--Purpose:
--	Called when an item needs to be picked up
--	and added to the player's inventory
function ply:pickupItem( item )
	--Make sure we're dealing with a valid entity
	if not item:IsValid() then return end
	
	local newItem = {}
	newItem.class = item:GetClass()
	newItem.name = item:GetName() or "N/A"
	newItem.mdl = item:GetModel()
	
	--Need to find a way to grab the "ENT.Purpose" of an item
	newItem.desc = "N/A"
	
	--This should be substituted with quickPutInBackpack once it's no longer a stub
	self:quickPutInBackpack( newItem )
end

function ply:getfromBackpack( row, column )
	return self.backpack[row][column] or nil
end

function ply:useFromBackpack( row, column )
	--Make sure what you're trying to use works
	if not self.backpack[row][column][2] then return end

	--Give the player the weapon
	self:Give( self.backpack[row][column][2] )
	
	--Remove whatever is at the position
	self.backpack[row][column] = {}
end

function ply:dropFromBackpack( row, column )
	--Make sure what you're trying to use works
	if not self.backpack[row][column][2] then return end

	--Create the weapon in front of the player
	local wep = ents.Create( self.backpack[row][column][2] )
	wep:SetPos( self:GetPos() )
	wep:Spawn()
	
	--Remove whatever is at the position
	self.backpack[row][column] = {}
end

--Function:
--	quickPutInBackpack
--Purpose:
--	Adds an item to the next avaliable free slot
--	in the player's backpack, returns false if there's
--	no slot avaliable
--Arguments:
--	Table containing
--		tbl.name
--		tbl.class
--		tbl.mdl
--		tbl.desc
function ply:quickPutInBackpack( ent )
	--Make sure we have the required information
	if not ent.name or not ent.class or not ent.mdl or not ent.desc then return end
	
	--First find the first available slot
	local aRow = false
	local aCol = false
	
	for row, col in pairs( self.backpack ) do
		for k,v in pairs( col ) do
			if not v["class"] and not aRow and not aCol then
				aRow = row
				aCol = k
			end
		end
	end
	
	--Make sure there's a space to put the item
	if not aRow or not aCol then return false end
	
	self.backpack[aRow][aCol] = ent
	self:updateBackpack()
end

--Search inventory to see if they own an entity, searches by name
function ply:hasInBackpack( name )
	for k,v in pairs( self.backpack ) do
		for key, value in pairs( v ) do
			if value["name"] == name then
				return k, key
			end
		end
	end
	return false
end

--Search inventory to see if they own an entity, searches by entity name
function ply:hasEntInBackpack( ent )
	for k,v in pairs( self.backpack ) do
		for key, value in pairs( v ) do
			if value["class"] == ent then
				return k, key, true
			end
		end
	end
	return false
end

--Function:
--	putInBackpack
--Purpose:
--	Adds an entity to the player's backpack
--	at a specified slot
function ply:putInBackpack( row, column, item )
	--Make sure that there's some space
	if self.backpack[row][column] then return end

	self.backpack[row][column] = {
		item.name, item.class, item.mdl, item.desc
	}
end

--Function:
--	updateBackpack
--Purpose:
--	Updates the client with any backpack changes
--	that happened serverside
function ply:updateBackpack()
	net.Start( "updateBackpack" )
		net.WriteTable( self.backpack )
	net.Send( self )
end

--When the backpack is first created, send everything
--to do the client, once they've fully loaded in
net.Receive("updateBackpackInitial", function( l, ply )
	ply:updateBackpack()
end)

--When the client wants to move an item from the inventory
--to the backpack
net.Receive("inventoryToBackpack", function( l, ply )
	local item = net.ReadTable()
	local invSlot = net.ReadInt( 8 )
	local vert = net.ReadInt( 8 )
	local horiz = net.ReadInt( 8 )
	
	--Make sure there's an item to move and a slot free
	if ply.inventory[invSlot]["class"] and not ply.backpack[vert][horiz]["class"] then
		local old = ply.inventory[invSlot]
		
		--Remove the weapon the player had
		ply:StripWeapon( old["class"] )
		
		--Delete it from their inventory and add it to their backpack
		ply.inventory[invSlot] = {}
		ply.backpack[vert][horiz] = old
	end
end)

--When the client wants to move an item from their backpack
--to another slot in their backpack
net.Receive("backpackToBackpack", function( l, ply )
	local oldVert = net.ReadInt( 8 )
	local oldHoriz = net.ReadInt( 8 )
	local newVert = net.ReadInt( 8 )
	local newHoriz = net.ReadInt( 8 )

	--Make sure they actually have an item in that slot
	if not ply.backpack[oldVert][oldHoriz]["class"] then return end
	
	--Make sure that there's nothing in the slot we're going to move to
	if ply.backpack[newVert][newHoriz]["class"] then return end
	
	--Move the slot over
	ply.backpack[newVert][newHoriz] = ply.backpack[oldVert][oldHoriz]
	
	--Empty the old slot
	ply.backpack[oldVert][oldHoriz] = {}
end)