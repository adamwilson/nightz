--[[

	The purpose of this file is to add the item
	that the player picks up to their backpack

]]

hook.Add( "PlayerSpawn", "PickupTimeout", function( ply )
	ply.PickupTimeout = CurTime() + 0.5
end )

hook.Add( "PlayerCanPickupWeapon", "NoPickup", function( ply, wep )
	for k,v in pairs( ply.inventory ) do
		if v["class"] == wep:GetClass() then
			return true
		end
	end
	
	if ( ( ply.PickupTimeout or 0 ) < CurTime() ) then return false end
end )

hook.Add( "KeyPress", "PressUse", function( ply, key )
	if ( key == IN_USE ) then
		local tr = ply:GetEyeTrace()
		if ( IsValid( tr.Entity ) and tr.Entity:IsWeapon() and tr.Entity:GetPos():Distance( ply:GetShootPos() ) < 96 ) then
			ply.PickupTimeout = CurTime() + 0.5
			
			local item = {}
			item.name = "test"
			item.mdl = tr.Entity:GetModel()
			item.class = tr.Entity:GetClass()
			item.desc = "N/A"
			
			if ply:quickPutInBackpack( item ) then
				tr.Entity:Remove()
				return true
			end			
		end
	end
end )