local menuColors = {
	Black 		= Color(0, 0, 0, 255),
	TBlack 		= Color(50, 50, 50, 200),
	TBlackBG	= Color(25, 25, 25, 200),
	Grey 		= Color(100, 100, 100, 255),
	TGrey		= Color(100, 100, 100, 200 ),
	Green 		= Color(39, 174, 96, 255 ),
	TGreen 		= Color(39, 174, 96, 20 ),
}
 
function createBackpack()
	backpackFrame = vgui.Create("DFrame")
	backpackFrame:SetSize(700, 427 )
	backpackFrame:Center()
	backpackFrame:ShowCloseButton( false )
	backpackFrame:MakePopup()
	backpackFrame:SetVisible( false )
	backpackFrame:SetTitle("")
	backpackFrame.Paint = function( s )
		draw.RoundedBox(0, 0, 0, s:GetWide(), s:GetTall(), nightz.colours["GreyAlpha"])
		draw.RoundedBox(0, 5, 5, s:GetWide()-10, s:GetTall()-10, nightz.colours["DarkGreyAlpha"])
	end
	
	local plyModel = vgui.Create("DModelPanel", backpackFrame)
	plyModel:SetModel( LocalPlayer():GetModel() )
	plyModel:SetSize( 500, 600 )
	plyModel:SetPos( 40, -105 )

	local Head = vgui.Create("DPanelSelect", backpackFrame)
	Head:SetPos(10,10)
	Head:SetSize(132,132)
	Head.Paint = function(s )
		draw.RoundedBox(0, 0, 0, s:GetWide(), s:GetTall(), menuColors.TBlack)
		draw.SimpleText( "Head", "Default", s:GetWide()/2, s:GetTall()/2, menuColors.Grey, 1, 1 )
	end
	
	local Body = vgui.Create("DPanelSelect", backpackFrame)
	Body:SetPos(10,147)
	Body:SetSize(132,132)
	Body.Paint = function(s )
		draw.RoundedBox(0, 0, 0, s:GetWide(), s:GetTall(), menuColors.TBlack)
		draw.SimpleText( "Vest", "Default", s:GetWide()/2, s:GetTall()/2, menuColors.Grey, 1, 1 )
	end
	
	local Legs = vgui.Create("DPanelSelect", backpackFrame)
	Legs:SetPos(10,285)
	Legs:SetSize(132,132)
	Legs.Paint = function(s )
		draw.RoundedBox(0, 0, 0, s:GetWide(), s:GetTall(), menuColors.TBlack)
		draw.SimpleText( "Legs", "Default", s:GetWide()/2, s:GetTall()/2, menuColors.Grey, 1, 1 )
	end
	
	backpack = vgui.Create("DPanelSelect", backpackFrame)
	backpack:SetPos(410, 7)
	backpack:SetSize(280,470)
	
	local total = defaultConfig.backpackSize
	local rows = total/4
	
	for i=1, rows do
		local row = vgui.Create("DPanelSelect", backpack)
		row:SetSize( backpack:GetWide(), 72 )
		
		backpack:AddItem( row )
		
		for i=1,4 do
			local slot = vgui.Create("DPanelSelect")
			slot:SetSize( 68,68 )
			slot.Paint = function( s )
				surface.SetDrawColor( menuColors.TBlack )
				surface.DrawRect( 0, 0, s:GetWide(), s:GetTall() )
			end
			
			slot:Receiver( "slot", function( pnl, tbl, dropped, menu, x, y )
				if ( !dropped ) then return end
				
				local action
				
				--Check if the item is coming from the inventory
				local invSlot
				for k,v in pairs( hotbarInventory:GetItems() ) do
					for key, val in pairs( v:GetItems() ) do
						if val == tbl[1] then
							invSlot = k
						end
					end
				end
				
				local vert, horiz
				for k,v in pairs( backpack:GetItems() ) do
					for key, val in pairs( v:GetItems() ) do
						if val == pnl then
							vert, horiz = k, key
						end
					end
				end
				
				if invSlot and vert and horiz then
					action = true
					inventoryToBackpack( tbl[1], invSlot, vert, horiz )
				end
				
				--Check if the item is coming from the backpack				
				local newVert, newHoriz, oldVert, oldHoriz
				for k,v in pairs( backpack:GetItems() ) do
					for key, val in pairs( v:GetItems() ) do
						--We need to go deeper, surely there's a better way of doing this?
						for keys, values in pairs( val:GetItems() ) do
							if values == tbl[1] then
								oldVert, oldHoriz = k, key
							end
						end
						
						if val == pnl then
							newVert, newHoriz = k, key
						end
					end
				end
				
				if oldVert and oldHoriz and newVert and newHoriz then
					backpackToBackpack( oldVert, oldHoriz, newVert, newHoriz )
					action = true
				end
				
				--This prevents us dropping the same item to the end of the current backpack
				local curItems = pnl:GetItems()
				if curItems[table.Count(curItems)] == tbl[1] then return end
				--If it's already in this panel then ignore it
				if table.HasValue( curItems, tbl[1] ) then return end
				
				if action then
					tbl[1]:SetSize( 64, 64 )
					pnl:AddItem(tbl[1])
				end
			end )
			
			row:AddItem( slot )		
		end
	end

	Head:Receiver( "slot", function( pnl, tbl, dropped, menu, x, y )
    	if ( !dropped ) then return end
		
		--This prevents us dropping the same item to the end of the current backpack
		local curItems = pnl:GetItems()
		if curItems[table.Count(curItems)] == tbl[1] then return end
		--If it's already in this panel then ignore it
		if table.HasValue( curItems, tbl[1] ) then return end
		
		--If there's already something in there then swap it out
		if #curItems > 0 then
			pnl:RemoveItem( curItems[1] )
		end
		
		tbl[1]:SetSize( Head:GetWide(), Head:GetTall()-4 )
		pnl:AddItem(tbl[1])
    end )
	
	Body:Receiver( "slot", function( pnl, tbl, dropped, menu, x, y )
    	if ( !dropped ) then return end
		
		--This prevents us dropping the same item to the end of the current backpack
		local curItems = pnl:GetItems()
		if curItems[table.Count(curItems)] == tbl[1] then return end
		--If it's already in this panel then ignore it
		if table.HasValue( curItems, tbl[1] ) then return end
		
		--If there's already something in there then swap it out
		if #curItems > 0 then
			pnl:RemoveItem( curItems[1] )
		end
		
		tbl[1]:SetSize( Head:GetWide(), Head:GetTall()-4 )
		pnl:AddItem(tbl[1])
    end )
	
	Legs:Receiver( "slot", function( pnl, tbl, dropped, menu, x, y )
    	if ( !dropped ) then return end
		
		--This prevents us dropping the same item to the end of the current backpack
		local curItems = pnl:GetItems()
		if curItems[table.Count(curItems)] == tbl[1] then return end
		--If it's already in this panel then ignore it
		if table.HasValue( curItems, tbl[1] ) then return end
		
		--If there's already something in there then swap it out
		if #curItems > 0 then
			pnl:RemoveItem( curItems[1] )
		end
		
		tbl[1]:SetSize( Head:GetWide(), Head:GetTall()-4 )
		pnl:AddItem(tbl[1])
    end )
	
	net.Start("updateBackpackInitial")
	net.SendToServer()
end

--Function:
--	backpackToBackpack
--Purpose:
--	Helper function for moving an item from
--	one slot in the backpack to another slot
--	in the backpack
--Arguments:
--	The vertical position of the item to move
--	The horizontal position of the item to move
--	The vertical position of the backpack slot to move the item to
--	The horizontal position of the backpack slot to move the item to
function backpackToBackpack( oldVert, oldHoriz, newVert, newHoriz )
	net.Start("backpackToBackpack")
		net.WriteInt( oldVert, 8 )
		net.WriteInt( oldHoriz, 8 )
		net.WriteInt( newVert, 8 )
		net.WriteInt( newHoriz, 8 )
	net.SendToServer()
end

--Function:
--	inventoryToBackpack
--Purpose:
--	Helper function for moving an item from
--	the inventory to the backpack
--Arguments:
--	DModelPanel object of item
--	Inventory slot to move to the backpack
--	Vertical backpack slot
--	Horizontal backpack slot
function inventoryToBackpack( item, invSlot, vert, horiz )
	
	local newItem = {}
	newItem.class = item.class
	newItem.mdl = item.mdl
	newItem.name = item.name
	newItem.desc = item.desc

	net.Start("inventoryToBackpack")
		net.WriteTable( newItem )
		net.WriteInt( invSlot, 8 )
		net.WriteInt( vert, 8 )
		net.WriteInt( horiz, 8 )
	net.SendToServer()
end

--Function: 
--	addToBackpack
--Purpose:
--	Adds an item to the backpack (clientside/visually)
--Arguments:
-- A table containing:
--	tbl.name	The name of the item
--	tbl.desc	Small description
--	tbl.mdl		Model path
--	tbl.class	The actual entity
--The vertical position in the table
--The horizontal position in the table to add to
function addToBackpack( item, vert, horiz )
	--Make sure what we're adding is actually a table
	if item.name and item.desc and item.mdl and item.class then
		local newItem = vgui.Create("DModelPanel")
		newItem:SetSize(64,64)
		newItem:Droppable("slot")
		newItem:SetModel( item.mdl )
		
		newItem.name = item.name
		newItem.mdl = item.mdl
		newItem.desc = item.desc
		newItem.class = item.class
		
		--Thanks to Giraffen93
		--http://facepunch.com/showthread.php?t=1443651&p=46806108&viewfull=1#post46806108
		local pnl = baseclass.Get("DModelPanel")
		newItem.Paint = function(self,w,h)
			surface.SetDrawColor( menuColors.TGreen )
			surface.DrawRect( 0, 0, self:GetWide(), self:GetTall() )
			pnl.Paint(self, w, h)
		end
		
		function newItem:LayoutEntity( ent ) end
		
		GenerateView( newItem )

		local slot = backpack:GetItems()[vert]
		local slot = slot:GetItems()[horiz]
		slot:AddItem( newItem )
	end
end

--Function:
--	showBackpack
--Purpose:
--	If the backpack window isn't visible
--	then make it visible, if it doesn't
--	exist then it'll create it
function showBackpack()
	if not backpackFrame then
		createBackpack()
		backpackFrame:SetVisible( true )
	else
		if not backpackFrame:IsVisible() then
			backpackFrame:SetVisible( true )
		end
	end
end

--Function:
--	hideBackpack
--Purpose:
--	If the backpack window is drawn, hide it
function hideBackpack()
	if not backpackFrame then return end
	
	if backpackFrame:IsVisible() then
		backpackFrame:SetVisible( false )
	end
end

--Updates that come from the server about the
--state of the client's inventory
net.Receive("updateBackpack", function()
	nightz.backpack = net.ReadTable()
	
	--Before adding new things we need to clear the current
	--backpack to make sure we don't show duplicates
	for k,v in pairs( backpack:GetItems() ) do
		for key, val in pairs( v:GetItems() ) do
			val:Clear()
		end
	end
	
	for k,v in pairs( nightz.backpack ) do
		for key, val in pairs( v ) do
			if val.name and val.class and val.mdl then
				local item = {}
				item.name = val["name"]
				item.class = val["class"] 
				item.mdl = val["mdl"]
				item.desc = val["desc"] or "NA"
				
				addToBackpack( item, k, key )
			end
		end
	end
end)
