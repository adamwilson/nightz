--[[

	The purpose of this file is to draw the character
	and provide character creation

]]

net.Receive("showCharCreationMenu", function()
	charCreation = vgui.Create( "DFrame" )
	charCreation:SetSize( 750, 400 )
	charCreation:Center()
	charCreation:SetTitle("")
	charCreation:SetBackgroundBlur( true )
	charCreation:ShowCloseButton( false )
	charCreation:SetDraggable( false )
	charCreation:MakePopup()
	
	function charCreation:Paint( w, h )
		Derma_DrawBackgroundBlur( charCreation, charCreation.startTime )
	end
	
	local charCreationBg = vgui.Create("DImage", charCreation )
	charCreationBg:SetSize( 750, 400 )
	charCreationBg:SetImage( "menu/stats_bg.png" )
	
	local charCreationHeader = vgui.Create("DLabel", charCreation)
	charCreationHeader:SetText("How will your story start?")
	charCreationHeader:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationHeader:SetPos( 365, 40 )
	charCreationHeader:SetSize( 300, 40 )
	charCreationHeader:SetFont( "MenuFont" )
	
	local charCreationBody = vgui.Create("DLabel", charCreation)
	charCreationBody:SetText("My name is")
	charCreationBody:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationBody:SetPos( 365, 80 )
	charCreationBody:SetSize( 300, 40 )
	charCreationBody:SetFont( "MenuBodyFont" )
	
	local charCreationFirstName = vgui.Create("DComboBox", charCreation)
	charCreationFirstName:SetPos( 475, 86 )
	charCreationFirstName:SetSize( 90, 25 )
	charCreationFirstName:SetFont( "MenuBodyFont" )
	for k,v in pairs( nightz.maleFirstNames ) do
		charCreationFirstName:AddChoice( v )
	end
	charCreationFirstName.Paint = function( w, h )
		draw.SimpleText( "...........", "tickButton", 0, -10, nightz.colours["DarkGrey"] )
	end
	
	local charCreationLastName = vgui.Create("DComboBox", charCreation)
	charCreationLastName:SetPos( 560, 86 )
	charCreationLastName:SetSize( 90, 25 )
	charCreationLastName:SetFont( "MenuBodyFont" )
	for k,v in pairs( nightz.lastNames ) do
		charCreationLastName:AddChoice( v )
	end
	charCreationLastName.Paint = function( w, h )
		draw.SimpleText( "...........", "tickButton", 0, -10, nightz.colours["DarkGrey"] )
	end
	
	local charCreationBody = vgui.Create("DLabel", charCreation)
	charCreationBody:SetText("I'm a          who used to work")
	charCreationBody:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationBody:SetPos( 365, 115 )
	charCreationBody:SetSize( 300, 40 )
	charCreationBody:SetFont( "MenuBodyFont" )	
	
	local charCreationGender = vgui.Create("DComboBox", charCreation)
	charCreationGender:SetPos( 415, 122 )
	charCreationGender:SetSize( 60, 25 )
	charCreationGender:SetFont( "MenuBodyFont" )
	charCreationGender:SetValue("guy")
	charCreationGender:AddChoice("guy")
	charCreationGender:AddChoice("girl")
	charCreationGender.Paint = function( w, h )
		draw.SimpleText( "........", "tickButton", 0, -10, nightz.colours["DarkGrey"] )
	end
	charCreationGender.OnSelect = function( panel, index, value )
		if value == "guy" then
			charCreationFirstName:Clear()
			for k,v in pairs( nightz.maleFirstNames ) do
				charCreationFirstName:AddChoice( v )
			end
		else
			charCreationFirstName:Clear()
			for k,v in pairs( nightz.femaleFirstNames ) do
				charCreationFirstName:AddChoice( v )
			end		
		end
	end	
	
	local charCreationBody = vgui.Create("DLabel", charCreation)
	charCreationBody:SetText("as a")
	charCreationBody:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationBody:SetPos( 365, 150 )
	charCreationBody:SetSize( 300, 40 )
	charCreationBody:SetFont( "MenuBodyFont" )
	
	local charCreationJob = vgui.Create("DComboBox", charCreation)
	charCreationJob:SetPos( 405, 157 )
	charCreationJob:SetSize( 90, 25 )
	charCreationJob:SetFont( "MenuBodyFont" )
	for k,v in pairs( nightz.jobs ) do
		charCreationJob:AddChoice( v["name"] )
	end
	charCreationJob.Paint = function( w, h )
		draw.SimpleText( "..............................", "tickButton", 0, -10, nightz.colours["DarkGrey"] )
	end
	charCreationJob.OnSelect = function( panel, index, value )
		surface.SetFont( "MenuBodyFont" )
		local tw, th = surface.GetTextSize( value )
		charCreationJob:SetSize( tw+30, 25 )
		
		charCreationJobText:SetText(nightz.jobs[index]["desc"])
	end
	
	local charCreationBody = vgui.Create("DLabel", charCreation)
	charCreationBody:SetText("It was much better before,")
	charCreationBody:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationBody:SetPos( 365, 220 )
	charCreationBody:SetSize( 300, 40 )
	charCreationBody:SetFont( "MenuBodyFont" )		
	
	charCreationJobText = vgui.Create("DLabel", charCreation)
	charCreationJobText:SetText("")
	charCreationJobText:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationJobText:SetPos( 365, 250 )
	charCreationJobText:SetSize( 300, 40 )
	charCreationJobText:SetFont( "MenuBodyFont" )	
	
	local charCreationBody = vgui.Create("DLabel", charCreation)
	charCreationBody:SetText("So much more peaceful...")
	charCreationBody:SetTextColor( nightz.colours["DarkGrey"] )
	charCreationBody:SetPos( 365, 340 )
	charCreationBody:SetSize( 300, 40 )
	charCreationBody:SetFont( "MenuBodyFont" )	
	
	acceptButton = vgui.Create( "DButton", charCreation )
	acceptButton:SetText( "" )
	acceptButton:SetSize( 50, 50 )
	acceptButton:SetPos( 610, 330 )
	acceptButton.DoClick = function()
		if charCreationFirstName:GetValue() == ""
			or charCreationLastName:GetValue() == ""
			or charCreationGender:GetValue() == ""
			or charCreationJob:GetValue() == ""
		then return end
		
		charCreation:Remove()
		
		net.Start( "finishCharCreationMenu" )
			net.WriteString( charCreationFirstName:GetValue() )	-- First
			net.WriteString( charCreationLastName:GetValue() )  -- Last
			net.WriteString( charCreationGender:GetValue() )  	-- Gender
			net.WriteString( charCreationJob:GetValue() )		-- Job
		net.SendToServer()
	end
	acceptButton.Paint = function()
		draw.SimpleText( "✔", "tickButton", 0, 0, nightz.colours["DarkGrey"] )
	end
end)

net.Receive("closeCharCreationMenu", function()
	if charCreationBg then
		charCreationBg:Remove()
	end
end)