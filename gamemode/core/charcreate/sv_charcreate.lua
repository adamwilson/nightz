--[[

	This file is for preventing the player spawning
	and for the showing/managing the character creation

]]

local ply = FindMetaTable("Player")

--Disable the player spawning until they've finished character creation
function GM:PlayerDeathThink( ply )
	return ply.inCharCreation
end

--We need this so that people can't suicide during the intro scene, and in general
function GM:CanPlayerSuicide( ply )
	return false
end

--Name: enableCharacterCreation
--Function: Notifies the server and client that
--the player is ready to be shown the character creation
--scene.
function ply:enableCharacterCreation()
	self.inCharCreation = true
	self:SetPos( nightz.spawnPos )
	self:SetEyeAngles( nightz.spawnAng )
	self:Freeze( true )
	self:StripWeapons()
	
	net.Start("showCharCreationMenu")
	net.Send( self )
end

--Name: finishCharacterCreation
--Function: Notifies the server and client that
--the player is done with character creation
function ply:finishCharacterCreation()
	self.inCharCreation = false
	self:Freeze( false )
	
	net.Start("closeCharCreationMenu")
	net.Send( self )

	self:fullySpawn()
end

net.Receive( "finishCharCreationMenu", function( l, ply )
	--Before we do anything check to see if they're in character creation
	if not ply.inCharCreation then return end
	
	local fName = net.ReadString()
	local lName = net.ReadString()
	local gender = net.ReadString()
	local job = net.ReadString()
	
	local fCheck, lCheck, jCheck
	
	--Check their first name and gender at the same time
	if gender == "guy" then
		if table.HasValue( nightz.maleFirstNames, fName ) then
			fCheck = true
		end
	elseif gender == "girl" then
		if table.HasValue( nightz.femaleFirstNames, fName ) then
			fCheck = true
		end		
	end
	
	--Don't bother going any further if they haven't got a legit name
	if not fCheck then return end
	
	--Check their last name
	if table.HasValue( nightz.lastNames, lName ) then
		lCheck = true
	end
	
	--Now check their job server side
	for k,v in pairs( nightz.jobs ) do
		if v["name"] == job then
			jCheck = true
			ply.job = v
		end
	end
	
	--If everything checks out then finish the character creation
	if fCheck and lCheck and jCheck then
		local name = fName .. " " .. lName
		ply:setName( name )
		
		local model
		if gender == "guy" then
			model = table.Random( nightz.maleModels )
			ply:SetModel( model )
		else
			model = table.Random( nightz.femaleModels )
			ply:SetModel( model )
		end
		
		ply:saveCharacterCreation( name, model, job )
		ply:finishCharacterCreation()
	end
end )