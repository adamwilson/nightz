--[[
	
	This file is for overwriting the default
	name functions in Garry's Mod so we can
	setup custom names
]]

--Find and store the metatable
local ply = FindMetaTable("Player")

if SERVER then
     function ply:setName(nickname)
          self:SetNWString("Nickname", nickname)
     end
end

if CLIENT then
	--We need to override this because the player will be called by their real name instead
	--of their renamed in game name, this might however break some chat prefix addons
	function GM:OnPlayerChat( player, strText, bTeamOnly, bPlayerIsDead )
		chat.AddText( player:Nick(), Color( 255, 255, 255 ), ": ", strText )
		return true
	end
end

--Override all the naming functions to return their character name
local oldplayernick = ply.Nick
function ply:Nick()
     return self:GetNWString("Nickname", oldplayernick(self))
end

local oldplayername = ply.Name
function ply:Name()
     return self:GetNWString("Nickname", oldplayernick(self))
end

local oldplayergetname = ply.GetName
function ply:GetName()
     return self:GetNWString("Nickname", oldplayernick(self))
end