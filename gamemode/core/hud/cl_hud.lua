--[[

	This file is for drawing/hiding HUD elements

]]

local toBeHidden = {
	CHudHealth = true,
	CHudBattery = true,
	CHudAmmo = true,
	CHudSecondaryAmmo = true,
	CHudWeaponSelection = true
}

hook.Add( "HUDShouldDraw", "hideHudElements", function( name )
	if toBeHidden[ name ] then
		return false
	end
end )

--When you pick up a weapon/item it draws on the
--side of the screen what you've picked up.
--Overriding the function disabled this as it's
--done in pure lua.
function GM:HUDItemPickedUp() end
function GM:HUDWeaponPickedUp() end
function GM:HUDAmmoPickedUp() end
