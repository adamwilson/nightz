/**

This file handles the initial connection
to the database and all relevant database
settings.

**/

-- Make sure you have the MySQLOO module:
-- http://facepunch.com/showthread.php?t=1357773
require( "mysqloo" )

--This is all the variables needed to connect to the database
--The variables below are taken from the config file.
local host = "localhost"
local user = "root"
local pass = ""
local dbname = "rp"
local port = 3306

gmDatabase = mysqloo.connect( host, user, pass, dbname, port )

--Function: 
--	rpDatabase:onConnected
--Description:
--	What happens when the database is initially connected
function gmDatabase:onConnected()
	Report( "Connected to the database" )
end

--Function:
--	rpDatabase:onConnectionFailed
--Description: 
--	What happens when the connection to the database fails
--	In this scenario we just print out that the connection failed along with
--	the error.
function gmDatabase:onConnectionFailed( err )
	Report( "Connection to database failed: " .. err )
end

-- After setting up what happens when we connect and what
-- happens when we fail to connect, then finally create
-- the connection itself.
gmDatabase:connect()