--[[

	The bleeding system works as follows:
	
	When the player takes damage there's a chance
	that they'll start to bleed, blood will drop on the 
	floor and give away player positions as well as
	hinder the player's vision.
	
	The player will need to bandage in order to remove
	the effects.

]]

--Pre-requisite for server->client networking
util.AddNetworkString("startBleeding")
util.AddNetworkString("stopBleeding")

--This chance is in terms of odds, 1 in X chance of bleeding
local chanceOfBleeding = 3

--Store the player metatable for player into ply for use later
local ply = FindMetaTable( "Player" )

hook.Add( "EntityTakeDamage", "applyBleeding", function( target, damage )
	--Lets make sure we're shooting a valid player
	if not target:IsValid() or not target:IsPlayer() then return end
	
	--Lets make sure the player isn't already bleeding
	if target:isBleeding() then return end

	local bleed = target.job.bleedingChance or chanceOfBleeding
	
	--Generate 2 numbers between 0 and the upper limit, if they match, start bleeding
	if math.random( 0, bleed ) == math.random( 0, bleed ) then
		Report("Starting bleed on " .. target:Nick())
		target:startBleeding()
	end
end)

--Function:
--	startBleeding
--Purpose:
--	Starts bleeding effects on the player
function ply:startBleeding()
	--If they're already bleeding no point going any further
	if self:isBleeding() then return end
	
	--Enable bleeding
	self:SetNWBool( "bleeding", true )
	
	Report( "Beginning bleeding on " .. self:Nick() )
	
	--Create the timer to spawn the particle effects
	timer.Create( "bleeding" .. self:EntIndex(), 3, 0, function()
		--Emit the bleeding effect
		local effectData = EffectData()
		effectData:SetOrigin( self:GetPos() + Vector( 0, 0, 45 ) )
		
		util.Effect( "BloodImpact", effectData, true, true )
	end )
	
	net.Start("startBleeding")
	net.Send( self )
end

--Function:
--	stopBleeding
--Purpose:
--	To disable the bleeding effects on the player
function ply:stopBleeding()
	self:SetNWBool( "bleeding", false )
	timer.Destroy( "bleeding" .. self:EntIndex() )
	
	--Let the client know to disable the effects
	net.Start("stopBleeding")
	net.Send( self )
end

--When the player dies, check if they're bleeding, if so then
--we can stop the bleeding, it's better to keep this in here
--rather than in the playerSpawn hook so if this module is removed
--then it won't result in an error
hook.Add( "PlayerDeath", "stopBleedingOnDeath", function( victim )
	if victim:isBleeding() then
		victim:stopBleeding()
		
		net.Start("stopBleeding")
		net.Send( victim )
	end
end )