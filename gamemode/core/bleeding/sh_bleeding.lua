/*

	Shared file	for the bleeding system.

*/

local ply = FindMetaTable("Player")

--Function:
--	isBleeding
--Purpose:
--	To check whether or not the player is bleeding
--Returns:
--	Boolean as to whether the player is bleeding
function ply:isBleeding()
	return self:GetNWBool( "bleeding" )
end

--Function:
--	spawnBloodDecal
--Purpose:
--	Helper function to spawn blood at the player's feet
--Arguments:
--	Player entity
function ply:spawnBloodDecal()
	util.PaintDown( self:GetPos(), "Blood", self )
end

--Function:
--  util.PaintDown
--Purpose: 
--  Helper function to paint decals on the floor
--Arguments:
--	Player
--	Name of the effect
--  Player
function util.PaintDown(start, effname, ignore)
   local btr = util.TraceLine({start=start, endpos=(start + Vector(0,0,-256)), filter=ignore, mask=MASK_SOLID})
   util.Decal(effname, btr.HitPos+btr.HitNormal, btr.HitPos-btr.HitNormal)
end

-- This timer spawns blood at the feet of players that are bleeding
-- every 4 seconds.
timer.Create( "spawnBloodDecals", 1, 0, function()
	for k,v in pairs( player.GetAll() ) do
		--Make sure they're bleeding
		if not v:isBleeding() then return end
		
		--If they're bleeding spawn a blood decal at their feet
		v:spawnBloodDecal()
	end
end )