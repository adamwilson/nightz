/*

	The clientside file handles the screen
	overlays and post-processing effects.

*/

--Function:
--	drawBlood
--Purpose:
--	Draw the blood overlay if the player is bleeding
function drawBlood()
	if LocalPlayer().bleeding then
		DrawMaterialOverlay( "effects/bleed_overlay", 0.01 )
	end
end
hook.Add( "RenderScreenspaceEffects", "Blood_Overlay", drawBlood )

--Handle the net message for start/stop bleeding
net.Receive("startBleeding", function()
	LocalPlayer().bleeding = true
end)

net.Receive("stopBleeding", function()
	LocalPlayer().bleeding = false
end)