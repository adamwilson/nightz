--[[

	The purpose of this file is to create
	the function to save player details
	to the database.

--]]

local ply = FindMetaTable( "Player" )


--Name:
--	savePlayer
--Purpose:
--	Saves the player's data to the server
function ply:savePlayer()
	local health = self:getHealth()
	local hunger = self:getHunger()
	local thirst = self:getThirst()
	local kills = self:GetNWInt("kills")
	local deaths = self:GetNWInt("deaths")
	
	local inv = util.TableToJSON( self.inventory )
	local back = util.TableToJSON( self.backpack )
	
	local status = {}
	status["wet"] = self:getWet()
	status["sick"] = self:getSick()
	status["brokenBones"] = self:getBrokenBones()
	status["bleeding"] = self:isBleeding()
	local status = util.TableToJSON( status )
	
	if gmDatabase then
		updatePlayer = gmDatabase:query("UPDATE players SET Health = "..health..", Hunger = "..hunger..", Thirst ="..thirst..", Kills = "..kills..", Deaths = "..deaths..",Status = '"..status.."', Inventory = '"..inv.."', Backpack = '"..back.."' WHERE SteamID = '"..self:SteamID().."'" )
		updatePlayer:start()
	else
		Report( "Cannot save player information, check your database settings" )
	end
end

--Name:
--	saveCharacterCreation
--Purpose:
--	Saves the player's data, that was selected in character
--  creation, to the server
function ply:saveCharacterCreation( name, model, job )
	if gmDatabase then
		updatePlayer = gmDatabase:query("UPDATE players SET Name = \""..name.."\", Model = \""..model.."\", Job = \""..job.."\" WHERE SteamID = \""..self:SteamID().."\"" )
		updatePlayer:start()
	else
		Report( "Cannot save player information, check your database settings" )
	end
end

--Make sure that when the player disconnects, all of their data
--is saved to the MySQL database. In here is where all the saving
--functions should be called.
hook.Add("PlayerDisconnected", "savePlayerOnDisconnect", function( ply )
	if ply:Alive() then
		--Save the player's information
		ply:savePlayer()
		
		--Save the logout position
		local pos = {}
		
		pos["x"] = ply:GetPos().x
		pos["y"] = ply:GetPos().y
		pos["z"] = ply:GetPos().z
		
		local pos = util.TableToJSON( pos )
		
		storeLogoutPos = gmDatabase:query("UPDATE players SET Logout = '".. pos .."' WHERE SteamID = '"..ply:SteamID().."'" )		
		storeLogoutPos:start()
		
		Report( "Saved logout position for: " .. ply:Nick() )
	end
end)