
local fancyrain = CreateClientConVar( "atmos_cl_rainsplash", 1, true, false );
local maxrain = CreateClientConVar( "atmos_cl_rainperparticle", 16, true, false );

function EFFECT:Init( data )
	
	self.Data = data;

	self.em3D = ParticleEmitter( data:GetOrigin(), true );
	self.em2D = ParticleEmitter( data:GetOrigin() );
	
	self.Live = true;

	atmos_log( "RainEffect init" );

end

local data, m, n, p, pos, ed;

function EFFECT:Think()
	
	data = self.Data;
	m = data:GetMagnitude();
	n = data:GetRadius();

	data:SetOrigin( LocalPlayer():GetPos() );

	if( !AtmosStorming ) then

		self:Die();

	end

	if( self.em3D ) then

		self.em3D:SetPos( data:GetOrigin() );
		
		for i = 1, maxrain:GetInt() do
			
			pos = data:GetOrigin() + Vector( math.random( -m, m ), math.random( -m, m ), math.random( m, 2 * m ) );
			
			if( atmos_Outside( pos ) ) then
				
				p = self.em3D:Add( "particle/water_drop", pos );
				
				p:SetAngles( Angle( 0, 0, -90 ) );
				p:SetVelocity( Vector( 0, 0, -1000 ) );
				p:SetDieTime( 5 );
				p:SetStartAlpha( 230 );
				p:SetStartSize( 4 );
				p:SetEndSize( 4 );
				p:SetColor( 255, 255, 255 );
				
				p:SetCollide( true );
				p:SetCollideCallback( function( p, pos, norm )
					
					if( render.GetDXLevel() > 90 and fancyrain:GetInt() > 0 and math.random( 1, 10 ) == 1 ) then
						
						ed = EffectData();
							ed:SetOrigin( pos );
						util.Effect( "atmos_rainsplash", ed );
						
					end
					
					p:SetDieTime( 0 );
					
				end );
				
			end
			
		end
		
	end

	if( self.em2D ) then
		
		self.em2D:SetPos( data:GetOrigin() );

		if( math.random() < 0.5 ) then
			
			pos = data:GetOrigin() + Vector( math.random( -m, m ), math.random( -m, m ), math.random( m, 2 * m ) );
			
			if( atmos_Outside( pos ) ) then
				
				p = self.em2D:Add( "effects/rainsmoke", pos );
				
				p:SetVelocity( Vector( 0, 0, -1000 ) );
				p:SetDieTime( 5 );
				p:SetStartAlpha( 6 );
				p:SetStartSize( 166 );
				p:SetEndSize( 166 );
				p:SetColor( 150, 150, 200 );
				
				p:SetCollide( true );
				p:SetCollideCallback( function( p, pos, norm )
					
					p:SetDieTime( 0 );
					
				end );
				
			end
			
		end
		
	end

	if( !self.Live ) then

		if( self.em3D ) then

			self.em3D:Finish();

		end

		if( self.em2D ) then

			self.em2D:Finish();

		end

		atmos_log( "RainEffect killed" );

		return self.Live;

	end

	return true;
	
end

function EFFECT:Die()

	self.Live = false;

end

function EFFECT:Render()
	
end
