nightz = {}

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "cl_fonts.lua" )
AddCSLuaFile( "cl_colours.lua" )
AddCSLuaFile( "sh_config.lua" )
AddCSLuaFile( "sh_jobs.lua" )
include( "shared.lua" )
include( "sv_networking.lua" )
include( "sh_config.lua" )

--Store player metatable for later reference
local ply = FindMetaTable("Player")

file.CreateDir( "nightz_logs" )

--Name: PInclude ( Print Include )
--Function: Rather than add another line of MsgC every time we want to
--	print an included file it's a whole lot easier to just override
--	the function and include MsgC every time.
function PInclude( string )
	MsgC( Color( 0, 255, 0 ), "[NightZ] Loaded " .. string .. "\n" )
	include( string )
	local str = Format("[NightZ @ %s] - Loaded %s\n", os.date(), string)
	file.Append("nightz_logs/nightz_" .. os.date( '%d_%m_%Y' ) .. ".txt", str)
end

--Name: Report
--Function: Rather than add a whole new line of MsgC for printing out
--debug functions such as 'database connected' it's easier to have it
--as one function in case anything needs to be changed later on
function Report( string )
	MsgC( Color( 0, 255, 0 ), "[NightZ] " .. string .. "\n" )
	local str = Format("[NightZ @ %s] - %s\n", os.date(), string)
	file.Append("nightz_logs/nightz_" .. os.date( '%d_%m_%Y' ) .. ".txt", str)
end

--Lets include all the files in the core folder
--Include all the sv_ and sh_ files and let the
--client download the cl_ and sh_ files. 
local fol = GM.FolderName.."/gamemode/core/"
local files, folders = file.Find(fol .. "*", "LUA")
for k,v in pairs(files) do
	if string.GetExtensionFromFilename(v) != "lua" then continue end
	PInclude(fol .. v)
end

for _, folder in SortedPairs(folders, true) do
	if folder == "." or folder == ".." then continue end

	for _, File in SortedPairs(file.Find(fol .. folder .."/sh_*.lua", "LUA"), true) do
		AddCSLuaFile(fol..folder .. "/" ..File)
		PInclude(fol.. folder .. "/" ..File)
	end

	for _, File in SortedPairs(file.Find(fol .. folder .."/sv_*.lua", "LUA"), true) do
		PInclude(fol.. folder .. "/" ..File)
	end

	for _, File in SortedPairs(file.Find(fol .. folder .."/cl_*.lua", "LUA"), true) do
		AddCSLuaFile(fol.. folder .. "/" ..File)
	end
end

function GM:PlayerSpawn( ply )	
	--Lets not spam the database with bots
	if ply:IsBot() then return end
	
	getPlayerInfo = gmDatabase:query( "SELECT Health, Hunger, Thirst, Inventory, Backpack, Kills, Deaths, Status, Name, Model, Job, Logout FROM players WHERE SteamID = '" .. ply:SteamID() .. "' LIMIT 1;" )
	
    function getPlayerInfo:onSuccess( data )
        if table.Count(data) > 0 then 
			-- If the returned table is greater than 1 entry then set
			-- the networked integers. 
			Report( ply:Nick() .. " loaded" )
			
			-- data[1] is the first table returned by the database
			-- You can then use data[1]["columnname"] to get
			-- individual pieces of data
			
			-- Set all the variables the player is going to use
			ply:SetHealth( data[1]["Health"] )
			ply:SetNWInt("thirst", data[1]["Thirst"] )
			ply:SetNWInt("hunger", data[1]["Hunger"] )
			ply:SetNWInt("kills", data[1]["Kills"] )
			ply:SetNWInt("deaths", data[1]["Deaths"] )
			ply.backpack = util.JSONToTable( data[1]["Backpack"] )
			
			--Give the player their inventory back
			ply.inventory = util.JSONToTable( data[1]["Inventory"] )

			--Set the details about the person
			ply:setName( data[1]["Name"] )
			ply:SetModel( data[1]["Model"] )
			
			for k,v in pairs( nightz.jobs ) do
				if v["name"] == data[1]["Job"] then
					ply.job = v
				end
			end
			
			if not ply.job then ply.job = {} end
			
			--Get the player's status effects and set them
			local statusEffects = util.JSONToTable( data[1]["Status"] )
			ply:SetNWBool( "wet", statusEffects["wet"] )
			ply:SetNWBool( "sick", statusEffects["sick"] )
			ply:SetNWBool( "brokenBones", statusEffects["brokenBones"] )
			
			if statusEffects["bleeding"] then
				--Give the player a few seconds to load in
				timer.Simple( 7, function()
					ply:startBleeding()
				end )
			end
			
			ply:fullySpawn()
			
			timer.Simple( 0, function()
				--Restore the player's position, there's a 0 second timer
				--because inside of the ply:fullySpawn it randomly selects a spawn point
				--it needs a delay so that ply:fullySpawn kicks in first, then this.
				local pos = util.JSONToTable( data[1]["Logout"] )
				local pos = Vector( pos["x"], pos["y"], pos["z"] )
				ply:SetPos( pos )
			end )
		else 
			-- If there's no data returned then create an entry for that user with the default info
			Report( ply:Nick() .. " not found in database, creating account")
			local NewUser = gmDatabase:query( "INSERT INTO players ( SteamID, Health, Thirst, Inventory, Backpack, Kills, Deaths, Status, Hunger ) VALUES ('" .. ply:SteamID() .. "', '" .. defaultConfig.health .. "', '" .. defaultConfig.thirst .. "', '" .. defaultConfig.inventory .. "', '" .. defaultConfig.backpack .. "', '0', '0', '" .. defaultConfig.status .. "', '" .. defaultConfig.hunger .. "'); " )
			NewUser:start()
			
			function NewUser:onError( err, sql )
				Report( "Query: " .. sql )
				Report( "Error: " .. err )	
			end
			
			--Since the player has joined for the very first time, set everything to default
			ply:SetHealth( defaultConfig.health )
			ply:SetNWInt("thirst", defaultConfig.thirst )
			ply:SetNWInt("hunger", defaultConfig.hunger )
			ply:SetNWInt("kills", 0 )
			ply:SetNWInt("deaths", 0 )
			ply.inventory = util.JSONToTable( defaultConfig.inventory )
			ply.backpack = util.JSONToTable( defaultConfig.backpack )
			
			ply:enableCharacterCreation()
		end
    end

    function getPlayerInfo:onError( err, sql )
		Report( "Query: " .. sql )
		Report( "Error: " .. err )	
    end

    getPlayerInfo:start()
	
	ply:SetupHands()
end

function ply:fullySpawn()
	self:SetWalkSpeed( nightz.walkSpeed )
	self:SetRunSpeed( nightz.runSpeed )
	
	for k,v in pairs( self.inventory ) do
		if v["class"] then
			self:Give( v["class"] )
		end		
	end
		
	--Send the player the latest inventory
	--Note that the backpack is done when
	--it's first opened
	self:updateInventory()
	
	--'Spawn' the player
	if nightz.useCustomSpawns then
		local ranPos = table.Random( nightz.customSpawns )
		self:SetPos( ranPos )
	else
		local ent = table.Random( ents.FindByClass("info_player_start") )
		local pos = ent:GetPos()
		self:SetPos( pos )
	end
end

function GM:PlayerNoClip(ply)
	if (IsValid(ply) and ply:IsPlayer() and ply:Alive()) then
		if (ply:IsAdmin() or ply:IsSuperAdmin()) then
			return true
		end
	
		return false
	end
end

MsgC( Color( 0, 255, 0 ), [[
===================================================================
  _   _ _       _     _   _____  _                    _          _ 
 | \ | (_) __ _| |__ | |_|__  / | |    ___   __ _  __| | ___  __| |
 |  \| | |/ _` | '_ \| __| / /  | |   / _ \ / _` |/ _` |/ _ \/ _` |
 | |\  | | (_| | | | | |_ / /_  | |__| (_) | (_| | (_| |  __/ (_| |
 |_| \_|_|\__, |_| |_|\__/____| |_____\___/ \__,_|\__,_|\___|\__,_|
          |___/   
						By Adzter & 44_oz
===================================================================
]] )