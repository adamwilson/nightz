--[[

	The purpose of this file is to hold all colour configurations
	to make it easier to change later.

]]
nightz.colours = {
	Green = Color(39, 174, 96, 255),
	White = Color(255, 255, 255),
	Grey = Color(199, 205, 209),
	DarkGrey = Color( 100, 100, 100 ),
	Red = Color( 255, 90, 90 ),
	GreyAlpha = Color( 50, 50, 50, 160 ),
	DarkGreyAlpha = Color( 10, 10, 10, 160 ),
}