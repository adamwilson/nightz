nightz = {}

include( "shared.lua" )
include( "cl_fonts.lua" )
include( "cl_colours.lua" )
include( "sh_config.lua" )

--Function:
--	GenerateView
--Purpose:
--	Automatically scale the model of a DModelPanel
--Arguments:
--	DModelPanel to scale
function GenerateView(Panel)
	local PrevMins, PrevMaxs = Panel.Entity:GetRenderBounds()
	Panel:SetCamPos(PrevMins:Distance(PrevMaxs)*Vector(0.7, 0.7, 0.25))
	Panel:SetLookAt((PrevMaxs + PrevMins)/2)
end

--Lets find and include all the files that we need
local root = GM.FolderName.."/gamemode/core/"
local _, folders = file.Find(root.."*", "LUA")

for _, folder in SortedPairs(folders, true) do
	for _, File in SortedPairs(file.Find(root .. folder .."/sh_*.lua", "LUA"), true) do
		include(root.. folder .. "/" ..File)
	end
	for _, File in SortedPairs(file.Find(root .. folder .."/cl_*.lua", "LUA"), true) do
		include(root.. folder .. "/" ..File)
	end
end

--Temporary debug
hook.Add( "HUDPaint", "drawDebug", function()
	draw.SimpleText( "Hunger: " .. LocalPlayer():getHunger(), "Default", 5, 10, Color( 255, 255, 255 ), 0, 0 )
	draw.SimpleText( "Thirst: " .. LocalPlayer():getThirst(), "Default", 5, 20, Color( 255, 255, 255 ), 0, 0 )
end )